# Import packages
import os
import pandas as pd
import numpy as np
from random import randint
from random import choices
import random
import matplotlib.pyplot as plt
from datetime import datetime
pd.options.display.max_columns=10

# Determine path for import and export
importpath2 = os.path.abspath("../Results/prop_conso.csv")
exportpath2 = os.path.abspath("../Results/")

#Read the table with the probabilities computed in Exploratory.py
prop_conso = pd.read_csv(importpath2, sep=";")
#Change the name of the first column
prop_conso.rename(columns={'Unnamed: 0':'hour'}, inplace=True)

## Create the classes

# We first create a class "Time" containing the timeslots
class Times(object):
    def __init__(self, timelist):
        self.timelist = timelist.hour.tolist()
        self.index_time = 0
        self.day = 0

# In this class we create a method that gives the timeslot
    def get_time(self):
        current_hour = self.timelist[self.index_time]
        self.index_time = (self.index_time + 1) % len(self.timelist)

        if self.index_time == 0:
            self.day += 1
        return current_hour

# In this class we create a method that gives the day from an index (not a date but we order the nb of days)
    def get_day(self):
        return self.day

# We create a class containing all consumptions possible (drinks and food)
# It also contains their prices
class Consumption(object):
    def __init__(self, drinkslist, foodlist):
        self.drinkslist = prop_conso.columns[1:7].tolist()
        self.foodlist = prop_conso.columns[7:12].tolist()
        self.price_d = {'milkshake': 5, 'frappucino': 4, 'water': 2, 'tea': 3, 'soda': 3, 'coffee': 3}
        self.price_f = {'sandwich': 2, 'cookie': 2, 'pie': 3, 'muffin': 3, 'NOFOOD': 0}

# Now, we create the class of the consumers
# We initialize the number of consumers in order to count the different ones and to generate the customerID
# The customerID is made by concatenate the 3 first letters of their subclass and their number (1st customer, 2nd...)
# The customers have also a budget and a tip as attributes
# Finally, with nb_order, we count the number of times that a customer order
# (to make the difference between One-Time and Returning customers)
class Customers(object):
    nb_customer = 0
    def __init__(self):
        self.budget = 0
        self.tip = 0
        Customers.nb_customer += 1
        self.customerID = type(self).__name__[:3] + str(Customers.nb_customer)
        self.nb_order = 0

# We define a method that restrict the customers:
# they are able to consume until their budget allows them to buy the more expensive drink plus the more expensive food
    def able_order(self):
        return self.budget >= 8

# The following method allows us to know what a customer bought conditionally on the timeslot
# For this, we use the table of the probas from the first part
# From the lists of drinks and food, and the probas
# We use the function "choices" that choose a consumptions following the distribution computed

    def place_order(self, consumption, times):
        if self.able_order():
            time = times.get_time()
            list_drinks = prop_conso.columns[1:7].tolist()
            list_d_proba = (prop_conso.loc[prop_conso['hour'] == time, 'frappucino':'milkshake']).iloc[0,:].tolist()
            drink_ordered = choices(list_drinks, list_d_proba)[0]
            self.budget -= consumption.price_d[drink_ordered] + self.tip
            self.nb_order += 1
            list_food = prop_conso.columns[7:12].tolist()
            list_f_proba = (prop_conso.loc[prop_conso['hour'] == time, 'NOFOOD':'cookie']).iloc[0,:].tolist()
            food_ordered = choices(list_food, list_f_proba)[0]
            self.budget -= consumption.price_f[food_ordered]

            return drink_ordered, food_ordered
        return None, None


## Now, we create the subclasses of customers

# One-Time Customers
# Particularity: they have a budget of 100
class OneTimeCust(Customers):
    def __init__(self):
        super().__init__()
        self.budget = 100
# They are able to order only one time
    def able_order(self):
        return (self.nb_order < 1) & (self.budget >= 8)

# Sub-class of One-time Customers: Customers that comes thanks to Trip Advisor
# Particularity: they give a tip between 1 and 10
class Tripadvisor(OneTimeCust):
    def __init__(self):
        super().__init__()
        self.tip = float(randint(1,10))

    def get_tip(self):
        return self.tip

# Returning Customers
# Particularity: they have a budget of 250
# We also want to get the history of consumption
class ReturningCust(Customers):
    def __init__(self):
        super().__init__()
        self.budget = 250
        self.history = []

# This method makes the history
    def place_order(self, consumption, times):
        order = super().place_order(consumption, times)
        self.history.append([order])

        return order

# They are able to order at least two times
    def able_order(self):
        return self.nb_order < 2 and self.budget >= 8

# Sub-class of Returning Customers: Hipsters
# Particularity: they have a budget of 500
class Hipster(ReturningCust):
    def __init__(self):
        super().__init__()
        self.budget = 500

    def place_order(self, consumption, times):
        order = super().place_order(consumption, times)
        self.history.append([order])

        return order

    def able_order(self):
        return self.nb_order < 2 and self.budget >= 8

###### SIMULATION

#Initialization
# Creation of objects of the class Times
timeslot = Times(prop_conso)
# Creation of objects of the class Consumption
conso = Consumption(prop_conso, prop_conso)
print(conso.foodlist)
#We create empty tables in which we are going to store information of:
#Income by day during 5 years
income = pd.DataFrame(np.zeros((1825, len(prop_conso['hour']))))
#Customers that ordered by day during 5 years
registerID = pd.DataFrame(np.zeros((1825, len(prop_conso['hour']))))

#We create a list of 1000 Returning customers of which 2/3 are Hipsters
list_returnC = [ReturningCust() if i < 667 else Hipster() for i in range(1000)]
#We need an empty list of income to store the income of a single day
list_income = []
#We need an empty list of customerID to store the customers of a single day
list_registerID = []
# We initialize the first day number to 0
day = 0
# We initialize the number of customers unabled to order to 0
unable_order_count = 0


# This is the loop that allows us to simulate the five years of life of the coffee bar
while timeslot.get_day() <= 1825: #1825 days in 5 years
    if len(list_returnC) <= unable_order_count:
        one_time_cust = 1 #One Time Cust. if number of Returning cust < number of cust that are not able to order
    else:
        one_time_cust = choices([0, 1], [0.2, 0.8])[0] #Proba to be a OneTime Cust.
    if one_time_cust:
        regular = choices([0, 1], [0.1, 0.9])[0] #Proba to be a  Regular OneTime Cust or (else) TripAdvisor.
        if regular:
            x = OneTimeCust()
        else:
            x = Tripadvisor()
    else:
        able_order = False
        while not able_order:
            x = random.choice(list_returnC)
            able_order = x.able_order() #If not able to order it is a returning customer
    drink, food = x.place_order(conso, timeslot) #gives the consumption of this customer
    list_income.append(conso.price_d[drink] + conso.price_f[food] + x.tip) #Store income
    list_registerID.append(x.customerID) #Store customerID
    if timeslot.get_day() != day: # Storage per day
        income.loc[day] = np.array(list_income)
        registerID.loc[day] = np.array(list_registerID)
        day += 1 #Next day
        list_income = [] #Initialization of the list for the next day
        list_registerID = []#Initialization of the list for the next day
    if not one_time_cust and not x.able_order():
        unable_order_count += 1 #No more budget if Returning




# Description & Plots

#Computation of total income per day
income_sum = np.sum(income, axis=1)
#Computation of average expenditure by customer per day
income_avg = np.mean(income, axis=1)

# Plot of total income per day
plot1= plt.figure(1)
plt.scatter(list(range(len(income_sum))), income_sum, marker="o")
plt.title("Total income per day")
plt.xlabel("Day")
plt.ylabel("Income")
plt.show()
plot1.savefig(os.path.join(exportpath2, 'Plot3.png'))

# Plot of average expenditure by customer per day
plot2 = plt.figure(2)
plt.scatter(list(range(len(income_avg))), income_avg, marker="*")
plt.title("Average expenditure per customer per day")
plt.xlabel("Day")
plt.ylabel("Average amount of an order")
plt.show()
plot2.savefig(os.path.join(exportpath2, 'Plot4.png'))

# Same plots for initial data
importpath = os.path.abspath("../Data/Coffeebar_2016-2020.csv")
# Read initial data
df = pd.read_csv(importpath, sep=";")
df['FOOD'] = df['FOOD'].fillna("NOFOOD")
# Create a variable extracting the year
df["YEAR"] = pd.to_datetime(df["TIME"]).dt.year
# Create a variable extracting the timeslot
df["HOUR"] = pd.to_datetime(df["TIME"]).dt.time
# Create a variable extracting the date
df["DAY"] = pd.to_datetime(df["TIME"]).dt.date

# We create columns containing prices
df["price_d"] = 0
df.loc[df['DRINKS']=="frappucino",'price_d']=4
df.loc[df['DRINKS']=="milkshake",'price_d']=5
df.loc[df['DRINKS']=="watter",'price_d']=2
df.loc[df['DRINKS']=="tea",'price_d']=3
df.loc[df['DRINKS']=="soda",'price_d']=3
df.loc[df['DRINKS']=="coffee",'price_d']=3
df["price_f"] = 0
df.loc[df['FOOD']=="sandwich",'price_f']=2
df.loc[df['FOOD']=="cookie",'price_f']=2
df.loc[df['FOOD']=="pie",'price_f']=3
df.loc[df['FOOD']=="muffin",'price_f']=3
# And a column contain the expenditure
df['income'] = df['price_d']+ df['price_f']

#Computation of total income per day
income_sum_df = df.groupby(by="DAY")["income"].sum()
#Computation of average expenditure by customer per day
income_avg_df = df.groupby(by="DAY")["income"].mean()
print(df.head())
print(income_sum_df.head())
print(income_avg_df.head())

# Plot of total income per day in the initial data
plot3 =plt.figure(3)
plt.scatter(list(range(len(income_sum_df))), income_sum_df, color="red", marker="o")
plt.title("Total income per day")
plt.xlabel("Day")
plt.ylabel("Income")
plt.show()
plot3.savefig(os.path.join(exportpath2, 'Plot5.png'))

# Plot of average expenditure by customer per day in the initial data
plot4 = plt.figure(4)
plt.scatter(list(range(len(income_avg_df))), income_avg_df, color="red", marker="*")
plt.title("Average expenditure per customer per day")
plt.xlabel("Day")
plt.ylabel("Average amount of an order")
plt.show()
plot4.savefig(os.path.join(exportpath2, 'Plot6.png'))

### PART 4

# Histories of returning customers


# Returning customers in initial data
# Table with only the returning customers
df_return = df.loc[df['CUSTOMER'].duplicated()==True]
#Number of customers
print(len(df['CUSTOMER'].unique()))
#Number of reurning customers
print(len(df_return['CUSTOMER'].unique()))

# We create a variable equal to 1 if the customer is a Retuning:
df['Returning']=df['CUSTOMER'].duplicated()
df.loc[df['Returning']== True,'Returning']=1
df.loc[df['Returning']== False,'Returning']=0

# We need the list of the timeslots
df_hours = df['HOUR'].unique()
df_hours_list = list(df_hours)

# Specific times for Returning Customers ?
#Number of times each Returning Customer came
print(df.loc[df['Returning']==1, :].groupby(by="CUSTOMER")['HOUR'].count())
#Number of unique timeslots each Returning Customer came
print(df.loc[df['Returning']==1, :].groupby(by="CUSTOMER")['HOUR'].nunique())
# Average proportion of coming on a specific timeslot for Returning Customers
print(((df.loc[df['Returning']==1, :].groupby(by="CUSTOMER")['HOUR'].nunique())/(df.loc[df['Returning']==1, :].groupby(by="CUSTOMER")['HOUR'].count())).mean())

# Example of the proportion of Returning Customers are 8am
print(((df.loc[(df['HOUR'] == df_hours[0]) & (df['Returning'] == 1), ['CUSTOMER']].count()) / (df.loc[df['HOUR'] == df_hours[0], ['CUSTOMER']].count())) * 100)

# Dataframe of proportion Of Retuning Customers and One-Time Customers for each timeslot
prop_type_cust= pd.DataFrame(columns=['Returning','OneTime'], index=df_hours_list)
print(prop_type_cust.head())

for t in df_hours_list:
    prop = round(((df.loc[(df['HOUR'] == t) & (df['Returning'] == 1), ['CUSTOMER']].count()) / (df.loc[df['HOUR'] == t, ['CUSTOMER']].count())) * 100,2)
    prop_type_cust.loc[prop_type_cust.index== t, 'Returning']=prop[0]

prop_type_cust['OneTime'] = 100-prop_type_cust['Returning']
print(prop_type_cust.head())

plot5 = plt.figure(5)
plt.bar(range(len(df_hours_list)),prop_type_cust['Returning'])
plt.title("Proportion of Returning Customers during the day")
plt.xlabel('Timeslots')
plt.ylabel('Proportion')
plt.show()
plot5.savefig(os.path.join(exportpath2, 'Plot7.png'))
print(df_hours_list[35])
print(df_hours_list[100])


#Table of drinks for type of customers
ctab_cust_d=pd.crosstab(df['Returning'],df['DRINKS'],margins=True,normalize=True)
print(ctab_cust_d)
ctab_cust_d.to_csv(os.path.join(exportpath2, 'ctab_cust_d.csv'), sep=';')
#Table of food for type of customers
ctab_cust_f=pd.crosstab(df['Returning'],df['FOOD'],margins=True,normalize=True)
print(ctab_cust_f)
ctab_cust_f.to_csv(os.path.join(exportpath2, 'ctab_cust_f.csv'), sep=';')