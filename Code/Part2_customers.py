# Import packages
import os
import pandas as pd
import numpy as np
from random import randint
from random import choices

# Determine path for import and export
importpath2 = os.path.abspath("../Results/prop_conso.csv")
exportpath2 = os.path.abspath("../Results/")

#Read the table with the probabilities computed in Exploratory.py
prop_conso = pd.read_csv(importpath2, sep=";")

## Create the classes

# We first create a class "Time" containing the timeslots
class Times(object):
    def __init__(self, timelist):
        self.timelist = timelist.prop_conso['index'].tolist()
        self.index_time = 0
        self.day = 0

# In this class we create a method that gives the timeslot
    def get_time(self):
        current_hour = self.timelist[self.index_time]
        self.index_time = (self.index_time + 1) % len(self.timelist)

        if self.index_time == 0:
            self.day += 1
        return current_hour

# In this class we create a method that gives the day from an index (not a date but we order the nb of days)
    def get_day(self):
        return self.day

# We create a class containing all consumptions possibles (drinks and food)
# It also contains their prices
class Consumption(object):
    def __init__(self, drinkslist, foodlist):
        self.drinkslist = prop_conso.columns[1:6].tolist()
        self.foodlist = prop_conso.columns[7:11].tolist()
        self.price_d = {'milkshake': 5, 'frappucino': 4, 'water': 2, 'tea': 3, 'soda': 3, 'coffee': 3}
        self.price_f = {'sandwich': 2, 'cookie': 2, 'pie': 3, 'muffin': 3, 'zerofood': 0}

# Now, we create the class of the consumers
# We initialize the number of consumers in order to count the different ones and to generate the customerID
# The customerID is made by concatenate the 3 first letters of their subclass and their number (1st customer, 2nd...)
# The customers have also a budget and a tip as attributes
# Finally, with nb_order, we count the number of times that a customer order
# (to make the difference between One-Time and Returning customers)
class Customers(object):
    nb_customer = 0
    def __init__(self):
        self.budget = 0
        self.tip = 0
        Customers.nb_customer += 1
        self.customerID = type(self).__name__[:3] + str(Customers.nb_customer)
        self.nb_order = 0

# We define a method that restrict the customers:
# they are able to consume until their budget allows them to buy the more expensive drink plus the more expensive food
    def able_order(self):
        return self.budget >= 8

# The following method allows us to know what a customer bought conditionally on the timeslot
# For this, we use the table of the probas from the first part
# From the lists of drinks and food, and the probas
# We use the function "choices" that choose a consumptions following the distribution computed
# Using prices defined above, we are able to compute the amount of the order
# Also, we deduce this amount (plus the tip) from their budget

    def place_order(self, consumption, times):
        if self.able_order():
            time = times.get_time()
            list_drinks = prop_conso.columns[1:6].tolist()
            list_d_proba = prop_conso.loc(prop_conso['index'] == time)[1:6].tolist()
            drink_ordered = choices(list_drinks, list_d_proba)[0]
            self.budget -= consumption.price_d[drink_ordered] + self.tip
            self.nb_order += 1
            list_food = prop_conso.columns[7:11].tolist()
            list_f_proba = prop_conso.loc(prop_conso['index'] == time)[7:11].tolist()
            food_ordered = choices(list_food, list_f_proba)[0]
            self.budget -= consumption.price_f[food_ordered]
            amount = consumption.price_d[drink_ordered] + self.tip + consumption.price_f[food_ordered]

            return drink_ordered, food_ordered, amount
        return None, None


## Now, we create the subclasses of customers

# One-Time Customers
# Particularity: they have a budget of 100
class OneTimeCust(Customers):
    def __init__(self):
        super().__init__()
        self.budget = 100
# They are able to order only one time
    def able_order(self):
        return (self.nb_order < 1) & (self.budget >= 8)

# Sub-class of One-time Customers: Customers that comes thanks to Trip Advisor
# Particularity: they give a tip between 1 and 10
class Tripadvisor(OneTimeCust):
    def __init__(self):
        super().__init__()
        self.tip = float(randint(1,10))

    def get_tip(self):
        return self.tip

# Returning Customers
# Particularity: they have a budget of 250
# We also want to get the history of consumption
class ReturningCust(Customers):
    def __init__(self):
        super().__init__()
        self.budget = 250
        self.history = []

# This method makes the history
    def place_order(self, consumption, times):
        order = super().place_order(consumption, times)
        self.history.append([order])

        return order

# They are able to order at least two times
    def able_order(self):
        return self.nb_order >= 1 and self.budget >= 8

# Sub-class of Returning Customers: Hipsters
# Particularity: they have a budget of 500
class Hipster(ReturningCust):
    def __init__(self):
        super().__init__()
        self.budget = 500

    def place_order(self, consumption, times):
        order = super().place_order(consumption, times)
        self.history.append([order])

        return order

    def able_order(self):
        return self.nb_order >= 1 and self.budget >= 8




