# Import packages
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from datetime import datetime

from pandas import DataFrame

# Determine path for import and export
importpath = os.path.abspath("../Data/Coffeebar_2016-2020.csv")
exportpath = os.path.abspath("../Results/")

# Read the data
df = pd.read_csv(importpath, sep=";")

print(df.head(5))
print(df.describe)

# Determine the different types of food and drinks sold
print(df["DRINKS"].unique())
print(df["FOOD"].unique())

# Count the number of customer
print(df["CUSTOMER"].describe())

# Create variables to isolate firstly the year and secondly the timeslots
print(df["TIME"].dtypes)
# Create a variable extracting the year
df["YEAR"] = pd.to_datetime(df["TIME"]).dt.year
# Create a variable extracting the timeslot
df["HOUR"] = pd.to_datetime(df["TIME"]).dt.time

print(df.head(5))

# Barplot of Amount of sold foods by year
y_food = pd.DataFrame(df.groupby(by="YEAR")["FOOD"].count())
print(y_food)
plot1 = plt.figure(1)
plt.bar(y_food.index, y_food["FOOD"])
plt.title("Amount of sold food by year")
plt.show()
plot1.savefig(os.path.join(exportpath, 'Plot1.png'))

# Barplot of Amount of sold drinks per year
y_drinks = pd.DataFrame(df.groupby(by="YEAR")["DRINKS"].count())
print(y_drinks)
plot2 = plt.figure(2)
plt.bar(y_drinks.index, y_drinks["DRINKS"])
plt.title("Amount of sold drinks by year")
plt.show()
plot2.savefig(os.path.join(exportpath, 'Plot2.png'))

# Create a list containing all types of consumptions (food and drinks)
df['FOOD'] = df['FOOD'].fillna("NOFOOD")
# List of drinks
list1 = df["DRINKS"].unique()
print(list1)
# List of food
list2 = df["FOOD"].unique()
print(list2)

print(type(list1))
# Concatenation of the two lists
conso = np.concatenate((list1, list2))
print(conso)
conso_list = list(conso)
print(conso_list)
print(type(conso_list))

# Create a list of timeslots
hours = df['HOUR'].unique()
hours_list = list(hours)
print(type(hours_list))
print(hours_list[1:5])

## Test : Probabilty of frappucino at 8am
# nb of customers who took a frappucino at 8am
print(df.loc[(df['HOUR'] == hours[0]) & (df['DRINKS'] == 'frappucino'), ['CUSTOMER']].count())
# nb of total customers at 8am
print(df.loc[df['HOUR'] == hours[0], ['CUSTOMER']].count())
# proportion of customers at 8am who took a frappucino
print(((df.loc[(df['HOUR'] == hours[0]) & (df['DRINKS'] == 'frappucino'), ['CUSTOMER']].count()) / (
    df.loc[df['HOUR'] == hours[0], ['CUSTOMER']].count())) * 100)

## Construct a loop to have all proportions (for all conso and all hours)

# Output is a text
for i in conso_list:
    for t in hours_list[0:1]:
        prop = round(((df.loc[(df['HOUR'] == t) & ((df['DRINKS'] == i) | (df['FOOD'] == i)), ['CUSTOMER']].count()) / (
            df.loc[df['HOUR'] == t, ['CUSTOMER']].count())) * 100, 0)
        print('On average the probability of a customer at ' + str(t) + " buying " + str(i) + " is " + str(
            prop[0]) + " %.")

# We only run the loop for the two first timeslots because the output would be too long for all timeslots.

# Output is a dataframe
prop_conso= pd.DataFrame(columns=conso_list, index=hours_list)
print(prop_conso.head())

for i in conso_list:
    for t in hours_list:
        prop = round(((df.loc[(df['HOUR'] == t) & ((df['DRINKS'] == i) | (df['FOOD'] == i)), ['CUSTOMER']].count()) / (
            df.loc[df['HOUR'] == t, ['CUSTOMER']].count())), 2)
        prop_conso.loc[prop_conso.index== t, str(i)]=prop[0]



print(prop_conso.head())



# We export the table as a csv in order to store it because the loop is a bit long to run
prop_conso.to_csv(os.path.join(exportpath, 'prop_conso.csv'), sep=';')
