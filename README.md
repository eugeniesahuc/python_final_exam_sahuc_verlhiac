#  Python Exam - Coffee Bar

In this project, we simulate a Coffee Bar. Firstly, we explore some existing data. Then, we create the classes of customers and of consumptions. After that, we simulate 5 years of Coffee Bar activity. And finally, we explore our simulation.

## Getting Started

In this project you will find several directories:

Data: containing the initial data used

Documentation: all information related to the project. More precisely, the set-up and documents that helped us to construct the code.

Results: The different outputs (plots, graphs, tables...)

Code: The different .py files:

Exploratory.py: the code we used to explore initial data

Part2_customers.py: the code we used to create the classes of objects

Part3&4_simulations.py: the code that take the Part2's code back and allows to make simulations from it.
A first commit corresponding to part's 3 deadline (8/11/2020) had an error and did not run.
The new version (last commit) works

Part4_ReturningCust50.py : Redo the simulation of Part3&4_simulations.py but changes the number of Returning Customers to 50

Part4_Hipster_budget40.py:  Redo the simulation of Part3&4_simulations.py but changes the budget of Hipsters to 40

Part4_moreTripAdvisor.py : Redo the simulation of Part3&4_simulations.py but changes the proportion of TripAdvisor among One-Time Customers (from 10% to 50%)

WARNING !!!
The Python scripts for part 3 and 4 run over around 15 minutes each !!

## Authors

* Eugenie Sahuc
* Alexandra Verlhiac


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

